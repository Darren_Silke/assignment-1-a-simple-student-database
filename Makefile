# Makefile for compiling C++ source code.
# Darren Silke (SLKDAR001), 2015

CC=g++ # The compiler name.
CCFLAGS=-std=c++11 # Flags passed to compiler.

# The normal build rules.

Driver: Driver.o Utilities.o Database.o
	$(CC) $(CCFLAGS) Driver.o Utilities.o Database.o -o Driver

Driver.o: Driver.cpp
	$(CC) $(CCFLAGS) Driver.cpp -c
	
Utilities.o: Utilities.cpp Utilities.h
	$(CC) $(CCFLAGS) Utilities.cpp -c

Database.o: Database.cpp Database.h
	$(CC) $(CCFLAGS) Database.cpp -c

# Clean rule.

clean:
	rm -f *.o Driver

# Run rule.

run:
	./Driver
