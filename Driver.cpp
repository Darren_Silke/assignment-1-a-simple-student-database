/* 
 * File:   Driver.cpp
 * Author: Darren Silke
 *
 * Created on 23 February 2015, 3:16 PM
 */

#include <iostream>
#include <cstdlib>

#include "Database.h"
#include "Utilities.h"

using std::cout;
using std::cin;
using SLKDAR001::addStudent;
using SLKDAR001::readDatabase;
using SLKDAR001::saveDatabase;
using SLKDAR001::displayGivenStudentData;
using SLKDAR001::gradeStudent;
using SLKDAR001::newLine;
using SLKDAR001::clear;

/*
 * Main function. No Command-line arguments required.
 */
int main() 
{    
    cout << "System initialising...";
    readDatabase();
    bool exit = false;
    char selection;
    
    // Event loop to process the user's menu selection. This will loop 
    // indefinitely or until the user quits the program by typing 'q' into the 
    // input prompt. 
    while(!exit)
    {
        cout << "0: Add student\n"
             << "1: Read database\n"
             << "2: Save database\n"
             << "3: Display given student data\n"
             << "4: Grade student\n"
             << "q: Quit\nEnter a number (or q to quit) and press return...\n";
        cin >> selection;
        newLine();
        
        switch(selection)
        {
            case '0':
            {
                addStudent();            
                break;
            }             
            case '1':
            {
                readDatabase();
                break;
            }
            case '2':
            {
                saveDatabase();
                break;
            }
            case '3':
            {
                displayGivenStudentData();
                break;
            }
            case '4':
            {
                gradeStudent();
                break;
            }       
            case 'q':
            {
                exit = true;
                break;
            }
            case 'Q':
            {
                exit = true;
                break;
            }
            // Default case for when the user inputs an invalid option.
            default:
            {
                cout << "Invalid option!\n";
                clear();
                break;
            }         
        }  
    }
    return 0;
}
