/* 
 * File:   Utilities.cpp
 * Author: Darren Silke
 *
 * Created on 28 February 2015, 4:23 PM
 */

#include <string>
#include <iostream>
#include <cstdlib>

#include "Utilities.h"
#include "Database.h"

using std::string;
using std::cin;
using std::cout;

namespace SLKDAR001
{    
    void stringToUpper(string &s)
    {
        for(unsigned int l = 0; l < s.length(); l++)
        {
            s[l] = toupper(s[l]);
        }
    }
    void newLine()
    {
        char symbol;
        do
        {
            cin.get(symbol);
        } while(symbol != '\n');
    }  
    void clear()
    {         
        cout << "Enter any character followed by the return key to continue.\n";
        char temp;
        cin >> temp;
        newLine();
        // To clear the terminal in Windows.
        // system("cmd /c cls");
        // To clear the terminal in UNIX.
        system("clear");
    }
}
