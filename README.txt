Author: Darren Silke
Date: 1 March 2015

Name: Assignment 1 - A Simple Student Database

Description: This software implements a simple database of student records, which can be queried for student information. The database consists of fixed size records which contain student details.

Instructions:

1. Open terminal BASH.
2. Type 'make' to compile and link all C++ source files.
3. Use the makefile provided to run the Driver executable by typing 'make run'.
4. Follow the instructions that appear on the screen.
5. Evaluate output.
6. To remove the Driver executable and the .o files, type 'make clean'.

List Of Files:

1. README.txt - Information file.
2. Makefile - Used to compile and run the program.
3. Database.txt - Used to store the student records.
4. Utilities.h - Specifies utility (helper) functions used by the program.
5. Utilities.cpp - Implements the utility (helper) functions used by the program.
6. Database.h - Specifies the functions involved in the processing of student records used by the program.
7. Database.cpp - Implements the functions involved in the processing of student records used by the program.
8. Driver.cpp - Driver file where the main function lies. This file makes use of both the Utilities.cpp and Database.cpp functions.

TAKE NOTE OF THE .git FOLDER WHICH CONTAINS ALL INFORMATION RELATING TO THE USE OF GIT FOR A LOCAL REPOSITORY AS REQUIRED BY THIS COURSE.
