/* 
 * File:   Utilities.h
 * Author: Darren Silke
 *
 * Created on 28 February 2015, 4:12 PM
 */

#ifndef UTILITIES_H
#define	UTILITIES_H

#include <string>

using std::string;

namespace SLKDAR001
{    
    /*
     * Function to convert a string to uppercase. This is used for converting 
     * student numbers. It aids in the comparison of student numbers when 
     * checking for already existing students in the database.   
     */
    void stringToUpper(string &s);
    /*
     * Function to flush an input stream. This works by clearing a whole line of
     * input and ignoring every character in the line until the newline 
     * character is encountered. 
     */
    void newLine();
    /*
     * Function to clear the terminal after processing every menu option that is
     * input by the user.
     */
    void clear();
}
#endif	/* UTILITIES_H */
