/* 
 * File:   Database.h
 * Author: Darren Silke
 *
 * Created on 23 February 2015, 5:39 PM
 */

#ifndef DATABASE_H
#define	DATABASE_H

#include <string>

using std::string;

namespace SLKDAR001
{ 
    /*
     * Structure to store a student record consisting of a name, surname, 
     * student number and class record. 
     */
    struct StudentRecord
    {
        string name;
        string surname;
        string studentNumber;
        string classRecord;
        
    };
    /* 
     * Function to create a student record for a new student. If student already 
     * exists, the existing student record will be overwritten with the new 
     * data. The user is prompt to enter the new students first name, last name,
     * student number and class record. The class record is a string of space 
     * separated numbers which reflect various marks that the student has 
     * obtained during the year. For example: "54 66 72 34".
     */
    void addStudent();
    /* 
     * Function to read in all student records from a text file into memory. 
     * This function is called automatically at program startup. 
     */
    void readDatabase();
    /* 
     * Function to save all changes made to any student records in memory to the
     * database. This includes updating the database to reflect new student 
     * records that were added.
     */ 
    void saveDatabase();
    /* 
     * Function to display a student record for a given student. The function 
     * prompts the user to enter a student's student number. The information 
     * that is displayed is the student's first name, last name, student number 
     * and class record. The class record is a string of space separated numbers
     * which reflect various marks that the student has obtained during the 
     * year. For example: "54 66 72 34".
     */
    void displayGivenStudentData();
    /*
     * Function to display an average for a student based on a given student 
     * number. The average will be constructed by extracting all the numbers in
     * the class record string and averaging them.
     */
    void gradeStudent();  
}
#endif	/* DATABASE_H */
