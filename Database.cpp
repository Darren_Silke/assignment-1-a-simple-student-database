/* 
 * File:   Database.cpp
 * Author: Darren Silke
 *
 * Created on 23 February 2015, 6:00 PM
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <string>
#include <sstream>

#include "Database.h"
#include "Utilities.h"

using std::cout;
using std::cin;
using std::vector;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::istringstream;

namespace SLKDAR001
{
    vector<StudentRecord> studentRecords;
    
    void addStudent()
    {
        cout << "\nFunction 'addStudent()' called.\n";
        
        string name, surname, studentNumber, classRecord;        
        StudentRecord studentRecord;
                
        cout << "Enter new student's first name:\n";
        getline(cin, name);
        
        cout << "Enter new student's last name:\n";
        getline(cin, surname);
        
        cout << "Enter new student's student number:\n";
        cin >> studentNumber;    
        
        cout << "Enter new student's class record (separated by blank spaces):\n";
        newLine();
        getline(cin, classRecord); 
        
        stringToUpper(studentNumber);
        bool studentExists = false;
        bool exitLoop = false;
        int index;
        // Loop through the studentRecords vector to check if a student with the
        // student number input by the user already exists.
        for(int i = 0; i < studentRecords.size() && exitLoop == false; i++)
        {       
            if(studentRecords[i].studentNumber == studentNumber)
            {
                index = i;
                studentExists = true;
                exitLoop = true;
            }
        }      
        
        // If the student already exists, overwrite the old student data with 
        // the new student data.
        if(studentExists)
        {        
            studentRecord.name = name;
            studentRecord.surname = surname;
            studentRecord.studentNumber = studentNumber;
            studentRecord.classRecord = classRecord;
            studentRecords[index] = studentRecord;
        }
        // If the student does not already exist, create a new student record 
        // and push it on to the back of the studentRecords vector.
        else
        {        
            studentRecord.name = name;
            studentRecord.surname = surname;
            studentRecord.studentNumber = studentNumber;
            studentRecord.classRecord = classRecord;
            studentRecords.push_back(studentRecord);
        } 
        cout << "Student " << studentNumber << " has been added successfully.\n";
        clear();
    }
    void readDatabase()
    {
        cout << "\nFunction 'readDatabase()' called.\n";
        
        studentRecords.clear();        
        ifstream inStream("Database.txt");      
        if(inStream.fail())
        {
            cout << "Database input file opening failed.\n";
            exit(1);
        }
        
        string textFileLine;   
        istringstream iss;
        while(! inStream.eof())
        {
            // Read entire line of text and store in a string.
            getline(inStream, textFileLine);
            // This checks for a blank line of text which mostly occurs at the 
            // last line in text files. If it encounters this, the loop skips to 
            // the next iteration.
            if(textFileLine == "")
                continue;
         
            iss.clear();
            // Add the string containing a line of text to an input 
            // stringstream.
            iss.str(textFileLine);        
            vector<string> rawData;
            StudentRecord studentRecord;      
            
            // Extract each piece of data (token) from the input stringstream 
            // and add it to a string vector.   
            while(! iss.eof())
            {
                string token;
                getline(iss, token, ';');
                rawData.push_back(token);
            }   
         
            // Initialise the student record structure declared above and add it
            // to the studentRecords vector. 
            studentRecord.name = rawData[0];
            studentRecord.surname = rawData[1];
            studentRecord.studentNumber = rawData[2];
            studentRecord.classRecord = rawData[3];
            studentRecords.push_back(studentRecord);         
        }
        inStream.close();
        cout << "Database has been successfully read into memory.\n";
        clear();
    }
    void saveDatabase()
    {
        cout << "\nFunction 'saveDatabase()' called.\n";
        
        ofstream outStream("Database.txt");
        if(outStream.fail())
        {
            cout << "Database output file opening failed.\n";
            exit(1);
        }
        
        // Loop through the studentRecords vector and write each student 
        // record to the database text file. The original database text file is
        // overwritten with the new data contained in the studentRecords vector.
        for(int i = 0; i < studentRecords.size(); i++)
        {
            outStream << studentRecords[i].name << ";"
                      << studentRecords[i].surname << ";"
                      << studentRecords[i].studentNumber << ";"
                      << studentRecords[i].classRecord << ";" << endl;
        }
        outStream.close();
        
        cout << "All changes made have been successfully updated in the database.\n";
        clear();
    }
    void displayGivenStudentData()
    {
        cout << "\nFunction 'displayGivenStudentData()' called.\n";
        
        string studentNumber;
        cout << "Enter a student number:\n";
        cin >> studentNumber;
        newLine();
        stringToUpper(studentNumber);
        
        bool studentExists = false;
        bool exitLoop = false;
        int index;
        // Loop through the studentRecords vector to check if the student 
        // exists and extract the index of the position in the studentRecords
        // vector.
        for(int i = 0; i < studentRecords.size() && exitLoop == false; i++)
        {       
            if(studentRecords[i].studentNumber == studentNumber)
            {
                index = i;
                studentExists = true;
                exitLoop = true;
            }                
        }  
        
        // If the student exists, display the student's information.
        if(studentExists)
        {
            cout << "\nStudent data for student " << studentNumber << ":\n"
                 << "Name: " << studentRecords[index].name << endl
                 << "Surname: " << studentRecords[index].surname << endl
                 << "Student Number: " << studentRecords[index].studentNumber << endl
                 << "Class Record: " << studentRecords[index].classRecord << endl;
        }
        // If the student does not exist, display an appropriate error message.
        else
        {
            cout << "Student " << studentNumber << " does not exist!\n";
        }    
        clear();
    }
    void gradeStudent()
    {
        cout << "\nFunction 'gradeStudent()' called.\n";
                
        string studentNumber;
        cout << "Enter a student number:\n";
        cin >> studentNumber;
        newLine();
        stringToUpper(studentNumber);
        
        bool studentExists = false;
        bool exitLoop = false;
        int index;
        // Loop through the studentRecords vector to check if the student 
        // exists and extract the index of the position in the studentRecords
        // vector.
        for(int i = 0; i < studentRecords.size() && exitLoop == false; i++)
        {       
            if(studentRecords[i].studentNumber == studentNumber)
            {
                index = i;
                studentExists = true;
                exitLoop = true;
            }                
        }  
                   
        // If the student exists, compute the student's average grade and 
        // display it.
        if(studentExists)
        {
            double totalGrade = 0.0;
            double numberOfGrades = 0.0;
            double averageGrade;
            
            istringstream iss;
            iss.str(studentRecords[index].classRecord);
            while(! iss.eof())
            {
                double grade;
                iss >> grade;
                totalGrade += grade;
                numberOfGrades++;
            }
         
            averageGrade = totalGrade/numberOfGrades;
            cout << "Student " << studentNumber << " has an average grade of: " << averageGrade << endl;  
        } 
        // If the student does not exist, display an appropriate error message.
        else
        {
            cout << "Cannot compute grade average!\nStudent " << studentNumber << " does not exist!\n";
        }          
        clear();
    }
}
